#ifndef DISABLE_SEC_CRT_HPP

#include <crtdbg.h>
#include <stdlib.h>

/** \brief disable msvc secure_crt "misbehaviour"
 * msvc calls a so called "_invalid_parameter_handler", when "insecure" or "invalid"
 * parameters are given to certain functions. The default handler calls abort()
 * In order to be able to handle the return values of certain functions in string.h
 * this handler has to be replaced.
 * This class does this conveniently for the current block.
 */
class no_invalid_parameter_handler final
{
    _invalid_parameter_handler m_hdlr;
    int                        m_repmode;

public:
    /**
     * \brief constructor - store current crt_report mode and current invalid_parameter_handler
     */
    no_invalid_parameter_handler()
        : m_hdlr(_set_invalid_parameter_handler(
            [](const wchar_t*, const wchar_t*, const wchar_t*, unsigned int, uintptr_t) {}))
        , m_repmode(_CrtSetReportMode(_CRT_ASSERT, 0))
    {
    }
    /**
     * \brief destructor - restore current crt_report mode and current invalid_parameter_handler
     */
    ~no_invalid_parameter_handler()
    {
        _set_invalid_parameter_handler(m_hdlr);
        _CrtSetReportMode(_CRT_ASSERT, m_repmode);
    }
    no_invalid_parameter_handler(const no_invalid_parameter_handler&)             = delete;
    no_invalid_parameter_handler(const no_invalid_parameter_handler&&)            = delete;
    no_invalid_parameter_handler& operator=(const no_invalid_parameter_handler&)  = delete;
    no_invalid_parameter_handler& operator=(const no_invalid_parameter_handler&&) = delete;
};
#endif // DISABLE_SEC_CRT_HPP
