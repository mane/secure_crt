#include <stdio.h>
#include <string.h>

#include "disable_sec_crt.hpp"

int main(int argc, char* argv[])
{
    no_invalid_parameter_handler here;

    char dst[10] = "";
    char src1[]  = "12345";
    char src2[]  = "67890";

    if (strncat_s(dst, src1, strlen(src1)))
    {
        return EXIT_FAILURE;
    }
    printf("dst: %s\n", dst);
    if (strncat_s(dst, src2, strlen(src2)))
    {
        return EXIT_FAILURE;
    }
    printf("dst: %s\n", dst);

    return EXIT_SUCCESS;
}
